from flask import Flask, request, jsonify

app = Flask(__name__)

class Student:
    def __init__(self, id, firstName, lastName):
        self.id = id
        self.firstName = firstName
        self.lastName = lastName

students = [Student(1, "Guilherme", "Cruz"), Student(2, "Jorge", "Aragão")]
next_id = 3

# Recuperar um estudante existente
@app.route("/students/<int:id>", methods=["GET"])
def get_student_by_id(id):
    for student in students:
        if student.id == id:
            return jsonify({"id": student.id, "firstName": student.firstName, "lastName": student.lastName})
    return jsonify({"error": "Estudante não encontrado"}), 404

# Retornar todos os estudantes
@app.route("/students", methods=["GET"])
def get_all_students():
    student_data = []
    for student in students:
        student_data.append({"id": student.id, "firstName": student.firstName, "lastName": student.lastName})
    return jsonify(student_data)

# Criar um novo estudante
@app.route("/students", methods=["POST"])
def create_student():
    global next_id
    data = request.get_json()
    firstName = data.get("firstName")
    lastName = data.get("lastName")
    student = Student(next_id, firstName, lastName)
    students.append(student)
    next_id += 1
    return jsonify({"message": "Estudante criado com sucesso!", "id": student.id}), 201

# Atualizar um estudante existente
@app.route("/students/<int:id>", methods=["PUT"])
def update_student(id):
    data = request.get_json()
    firstName = data.get("firstName")
    lastName = data.get("lastName")
    for student in students:
        if student.id == id:
            student.firstName = firstName
            student.lastName = lastName
            return jsonify({"message": "Estudante atualizado com sucesso!", "id": student.id})
    return jsonify({"error": "Estudante não encontrado"}), 404

# Remover um estudante existente
@app.route("/students/<int:index>", methods=["DELETE"])
def delete_student(index):
    if index < len(students):
        del students[index]
        return jsonify({"message": "Estudante removido com sucesso!"})
    else:
        return jsonify({"error": "Índice inválido"}), 404

if __name__ == "__main__":
    app.run(debug=True)