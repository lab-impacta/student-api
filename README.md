# Student-API

Exemplo simples demonstrando o uso e propriedade dos métodos/verbos HTTP através da manipulação de dados em uma lista.


| Método | Objetivo                          | CRUD   | Request Body | Idempotente¹ | Seguro² |
| :-------- | ----------------------------------- | -------- | -------------- | --------------- | ---------- |
| GET     | Obter um recurso ou grupo         | Read   | Não         | Sim           | Sim      |
| POST    | Criar um novo recurso             | Create | Sim          | Não          | Não     |
| PUT     | Atualizar um recurso inteiro      | Update | Sim          | Sim           | Não     |
| DELETE  | Deletar um recurso                | Delete | Não         | Não          | Não     |
| PATCH   | Atualizar parcialmente um recurso | Update | Sim          | Sim           | Não     |

¹ Em Matemática ou Ciência da Computação, propriedade de uma operação ser aplicada mais de uma vez sem que haja alteração em seu resultado.

² Método que não altera um dado no lado servidor

![Alt text](image.png)

## Uso local

```
$ python -m flask run --host=0.0.0.0
```

## Uso com Postman

Importar a collection student-api-flask.postman_collection.json